# Generated by Django 3.2.7 on 2021-09-27 03:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='fromModel',
            new_name='Dari',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='titleModel',
            new_name='Judul',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='toModel',
            new_name='Kepada',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='messageModel',
            new_name='Pesan',
        ),
    ]
