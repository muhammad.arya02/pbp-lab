from django.db import models

# Create your models here.
class Note(models.Model):
    Kepada = models.CharField(max_length=30)
    Dari = models.CharField(max_length=30)
    Judul = models.CharField(max_length=30)
    Pesan = models.TextField()