from django.urls import path
from .views import index, add_note, note_list
from lab_2.models import Note


urlpatterns = [
    path('', index, name='index'),
    path('add_note', add_note, name='add_note'),
    path('note_list', note_list, name='note_list')
]
 