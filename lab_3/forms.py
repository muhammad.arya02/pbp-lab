from lab_1.models import Friend
from django import forms
from django.forms import fields

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"