1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

1. JSON (JavaScript Object Notation) merupakan sebuah format yang dapat digunakan untuk menyimpan dan mentransfer data. Sementara itu XML (Extensible Markup Language ) merupakan bahasa markup yang berfungsi untuk menyederhanakan proses penyimpanan dan pengiriman data antarserver.

Setelah mengetahui penjelasan singkatnya kita bisa membedakan JSON dan XML

> Dalam menyimpan data JSON lebih mudah dibaca dan lebih simpel ketimbang XML, penyimpanan  data menggunakan JSON layaknya dictionary dalam Python yang memiliki pasangan key dan value. Sementara itu XML menyimpan data dengan diapit tag yang menandakan jenis datanya/objeknya

Sintax JSON :
{"city":"New York", "country":"United States "}
Sintax XML :
<user>
    <username>Arya</username> <lokasi>Bekasi</lokasi>
</user>
<user>
    <username>Adi</username> <lokasi>Jakarta</lokasi>
</user>

> JSON mendukung penggunaan array secara langsung sementara XML tidak mendukung penggunaan array

> Dalam encoding, JSON hanya mendukung UTF-8 sementara XML mendukung berbagai macam peng-encoding-an

> JSON diyakini tidak seaman XML dalam urusan keamanan data security



2. Setelah penjelasan XML dan JSON diatas, kita masuk ke HTML. HTML (Hypertext Markup Language) merupakan bahasa markup yang digunakan dalam pembuatan struktur halaman web. 

Adapun perbedaan XML dan HTML adalah 

> XML menerapkan case sensitive sementara HTML tidak

> Jika XML digunakan untuk mentransfer data, HTML digunakan untuk menyajikan/menampilkan data. HTML tidak dapat digunakan untuk menyimpan data.

> XML menyediakan framework tetapi HTML tidak

> Tujuan digunakannya HTML untuk membuat tampilan pada website sementara XML untuk proses transfer data

> Walaupun sama-sama menerapkan tag pembuka dan penutup, HTML sudah ada ketentuan penggunaannya seperti <body></body> ataupun <title></title> sementara pada XML tag tersebut kita definisikan sendiri layaknya pembuatan suatu variabel yang kita definisikan sendiri.

https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.niagahoster.co.id/blog/html-adalah/
https://blogs.masterweb.com/perbedaan-xml-dan-html/
