// ignore_for_file: prefer_const_constructors, prefer_const_declarations

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/button_widget.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String title = 'Customer Support';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primaryColor: Colors.red),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String email = '';

  Widget Judul() => Text(
        "Dapatkan Informasi Mengenai Produk Kami",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(Icons.chat),
        ),
        bottomNavigationBar: Row(
          children: <Widget>[
            Container(
              height: 60,
              width: MediaQuery.of(context).size.width / 3,
              decoration: BoxDecoration(color: Colors.blue[300]),
              child: Icon(
                Icons.home,
                color: Colors.white,
              ),
            ),
            Container(
              height: 60,
              width: MediaQuery.of(context).size.width / 3,
              decoration: BoxDecoration(color: Colors.blue[300]),
              child: Icon(
                Icons.support_agent,
                color: Colors.white,
              ),
            ),
            Container(
              height: 60,
              width: MediaQuery.of(context).size.width / 3,
              decoration: BoxDecoration(color: Colors.blue[300]),
              child: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
            ),
          ],
        ),
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Form(
          key: formKey,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: [
              Judul(),
              const SizedBox(height: 16),
              buildNama(),
              const SizedBox(height: 16),
              buildEmail(),
              const SizedBox(height: 32),
              buildSubmit(),
            ],
          ),
        ),
      );

  Widget buildNama() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Nama',
          border: OutlineInputBorder(),
        ),
        maxLength: 50,
        onSaved: (value) => setState(() => nama = value!),
      );

  Widget buildEmail() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
          final regExp = RegExp(pattern);

          if (value!.isEmpty) {
            return 'Enter an email';
          } else if (!regExp.hasMatch(value)) {
            return 'Enter a valid email';
          } else {
            return null;
          }
        },
        keyboardType: TextInputType.emailAddress,
        onSaved: (value) => setState(() => email = value!),
      );

  Widget buildSubmit() => TextButton(
        child: Text("Subscribe"),
        onPressed: () {
          final isValid = formKey.currentState!.validate();

          if (isValid) {
            formKey.currentState!.save();

            final message = '$nama dengan email $email berhasil didaftarkan';
            print(nama);
            print(email);
            print(message);

            final snackBar = SnackBar(
              content: Text(
                message,
                style: TextStyle(fontSize: 20),
              ),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        style: TextButton.styleFrom(
            primary: Colors.white, backgroundColor: Colors.red),
      );
}
